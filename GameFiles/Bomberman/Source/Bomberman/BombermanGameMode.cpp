// Copyright Epic Games, Inc. All Rights Reserved.

#include "BombermanGameMode.h"
#include "BombermanPawn.h"
#include "Public/BombermanCharacter.h"

ABombermanGameMode::ABombermanGameMode()
{
	// set default pawn class to our character class
	DefaultPawnClass = ABombermanPawn::StaticClass();
}

