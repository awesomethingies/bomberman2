// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BombermanCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;

class ABomb;
class ABombRemote;

UCLASS()
class BOMBERMAN_API ABombermanCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABombermanCharacter();


	UFUNCTION()
	void RefillBomb(bool Value);

	UFUNCTION()
	void IncreaseBombRadius(int32 Value);
	UFUNCTION()
	void IncreaseBombAmmo(int32 Value);
	UFUNCTION()
	void IncreaseSpeed(float Value);
	UFUNCTION()
	void ActivateRemoteBomb();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	//Movement methods
	UFUNCTION()
	void MoveForward(float Value);
	UFUNCTION()
	void MoveRight(float Value);

	UFUNCTION()
	void DropBomb();


public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* CameraSpringArm = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* Camera = nullptr;

	UPROPERTY(EditAnywhere)
	int32 PlayerId = 0;

	UPROPERTY(EditAnywhere)
	int32 MaxBombAmmo = 1;
	UPROPERTY(EditAnywhere)
	int32 BombRadius = 1;
	UPROPERTY(EditAnywhere)
	bool RemoteBombUsage = false;
	UPROPERTY(EditAnywhere)
	float RemoteBombTime = 10;
	UPROPERTY(EditAnywhere)
	float RemoteBombTimer = 0;

	UPROPERTY(EditAnywhere)
	float TileLenght = 100;

private:

	int32 BombAmmo = 0;

	ABombRemote* RemoteBomb = nullptr;

};
