// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PowerUpBase.h"
#include "BombAmmoPowerup.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API ABombAmmoPowerup : public APowerUpBase
{
	GENERATED_BODY()
	
public:
	ABombAmmoPowerup();

	virtual void Activate(ABombermanCharacter* character) override;

public:
	int32 Value = 1;
};
