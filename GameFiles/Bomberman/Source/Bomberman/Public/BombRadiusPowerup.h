// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PowerUpBase.h"
#include "BombRadiusPowerup.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API ABombRadiusPowerup : public APowerUpBase
{
	GENERATED_BODY()

public:
	ABombRadiusPowerup();

	virtual void Activate(ABombermanCharacter* character) override;


public:
	int32 Value = 1;	
};
