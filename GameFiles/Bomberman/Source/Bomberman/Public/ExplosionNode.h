// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExplosionNode.generated.h"


UCLASS()
class BOMBERMAN_API AExplosionNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExplosionNode();

	void SetData(int _ExplosionForce, float _TileLenght, FVector _Direction);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	void Expand();


public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* ExplosionMesh;

	UPROPERTY(VisibleAnywhere)
	int ExplosionForce = 0;

	UPROPERTY(VisibleAnywhere)
	float TileLenght = 0.0f;

	UPROPERTY(VisibleAnywhere)
	FVector Direction = FVector(0,0,0);


};
