// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PowerUpBase.h"
#include "BombRemotePowerup.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API ABombRemotePowerup : public APowerUpBase
{
	GENERATED_BODY()
public:
	ABombRemotePowerup();

	virtual void Activate(ABombermanCharacter* character) override;

};
