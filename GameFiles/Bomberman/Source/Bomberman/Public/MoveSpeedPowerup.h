// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PowerUpBase.h"
#include "MoveSpeedPowerup.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API AMoveSpeedPowerup : public APowerUpBase
{
	GENERATED_BODY()
	

public:
	AMoveSpeedPowerup();

	virtual void Activate(ABombermanCharacter* character) override;

public:
	float Value = 100;
};
