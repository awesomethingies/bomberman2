// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridGenerator.generated.h"

class UStaticMeshComponent;
class UStaticMesh;

UCLASS()
class BOMBERMAN_API AGridGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridGenerator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


public:

	UPROPERTY(EditAnywhere)
	FVector2D Spacing = FVector2D(0,0);


	UPROPERTY(EditAnywhere)
	UStaticMesh* Tile = nullptr;
};
