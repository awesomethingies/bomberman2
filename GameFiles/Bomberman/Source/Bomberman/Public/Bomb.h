// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

//Parameter not needed, is useless, but i cannot make a nonParamater deleage work, it's ugly but it work
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBombExplodedEvent, bool, returnBool);

class UStaticMeshComponent;

UCLASS()
class BOMBERMAN_API ABomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomb();
	ABomb(float TimerValue, float TileLenght);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	

	void SetRadius(int32 Radius);

	void Explode();

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* BombMesh;

	UPROPERTY(BlueprintAssignable)
	FBombExplodedEvent OnBombExplosionEvent;

	UPROPERTY(VisibleAnywhere)
	int32 BombRadius = 1;

	UPROPERTY(VisibleAnywhere)
	float FuseTime = 0.0f;
	UPROPERTY(VisibleAnywhere)
	float FuseTimer = 0.0f;
};
