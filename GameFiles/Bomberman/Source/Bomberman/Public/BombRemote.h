// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bomb.h"
#include "BombRemote.generated.h"

//Simple dummy class to disable fuse tick, so the bomb is detonated remotly
//May be used to change texture later for diferentation
UCLASS()
class BOMBERMAN_API ABombRemote : public ABomb
{
	GENERATED_BODY()

public:
	ABombRemote();
};
