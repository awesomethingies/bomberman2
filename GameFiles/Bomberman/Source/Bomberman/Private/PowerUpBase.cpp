// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerUpBase.h"
#include "..\Public\PowerUpBase.h"
#include "BombermanCharacter.h"

// Sets default values
APowerUpBase::APowerUpBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> PowerUpAsset(TEXT("StaticMesh'/Game/Bomberman/Meshes/Tile.Tile'"));

	PowerUpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PowerUpMesh"));
	PowerUpMesh->SetStaticMesh(PowerUpAsset.Object);
	PowerUpMesh->SetupAttachment(RootComponent);
	PowerUpMesh->SetCollisionProfileName(TEXT("OverlapAll"));
	PowerUpMesh->OnComponentBeginOverlap.AddDynamic(this, &APowerUpBase::OnBeginOverlap);

	PowerUpMesh->SetRelativeScale3D(FVector(0.5, 0.5, 0.5));

}

// Called when the game starts or when spawned
void APowerUpBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APowerUpBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APowerUpBase::OnBeginOverlap(UPrimitiveComponent* OverlapComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA<ABombermanCharacter>())
	{
		Activate(CastChecked<ABombermanCharacter>(OtherActor));
		Destroy();
	}
}

void APowerUpBase::Activate(ABombermanCharacter* character)
{
}

void APowerUpBase::Deactivate(ABombermanCharacter* character)
{
}
