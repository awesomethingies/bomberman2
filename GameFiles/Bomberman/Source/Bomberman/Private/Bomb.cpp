// Fill out your copyright notice in the Description page of Project Settings.


#include "Bomb.h"
#include "ExplosionNode.h"

// Sets default values
ABomb::ABomb()
{
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> BombMeshAsset(TEXT("StaticMesh'/Game/Bomberman/Meshes/Bomb.Bomb'"));

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> BombMaterial(TEXT("Material'/Game/Bomberman/Meshes/BombMaterial.BombMaterial'"));


	BombMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BombMesh"));
	BombMesh->SetStaticMesh(BombMeshAsset.Object);
	BombMesh->SetupAttachment(RootComponent);
	BombMesh->SetCollisionProfileName(TEXT("OverlapAll"));
	BombMesh->SetMaterial(0, BombMaterial.Object);
	//RootComponent = ProjectileMesh;

	FuseTime = 3.0f;
}

// Sets default values
ABomb::ABomb(float TimerValue, float TileLenght)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> BombMeshAsset(TEXT("StaticMesh'/Game/Bomberman/Meshes/Bomb.Bomb'"));

	BombMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BombMesh"));
	BombMesh->SetStaticMesh(BombMeshAsset.Object);
	BombMesh->SetupAttachment(RootComponent);
	BombMesh->SetCollisionProfileName(TEXT("NoCollision"));
	//RootComponent = ProjectileMesh;

	FuseTime = TimerValue;
}

// Called when the game starts or when spawned
void ABomb::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	FuseTimer += DeltaTime;

	if (FuseTimer >= FuseTime)
	{
		Explode();
		FuseTimer = 0;
	}
}


void ABomb::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("bomb despawn"));


	OnBombExplosionEvent.Broadcast(true);
}




void ABomb::SetRadius(int32 Radius)
{
	BombRadius = Radius;
}

void ABomb::Explode()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Doing Explosion"));

	//Deactivate collisions to avoid a collision loop
	BombMesh->SetCollisionProfileName(TEXT("NoCollision"));

	//Create an explision node for each 4 directions
	AExplosionNode* ExplosionNodeUp = GetWorld()->SpawnActor<AExplosionNode>(GetActorLocation(), FRotator(0,0,0));
	ExplosionNodeUp->SetData(BombRadius, 100.0f, FVector(1, 0, 0));
	ExplosionNodeUp->Expand();
	AExplosionNode* ExplosionNodeDown = GetWorld()->SpawnActor<AExplosionNode>(GetActorLocation(), FRotator(0, 0, 0));
	ExplosionNodeDown->SetData(BombRadius, 100.0f, FVector(-1, 0, 0));
	ExplosionNodeDown->Expand();
	AExplosionNode* ExplosionNodeRight = GetWorld()->SpawnActor<AExplosionNode>(GetActorLocation(), FRotator(0, 0, 0));
	ExplosionNodeRight->SetData(BombRadius, 100.0f, FVector(0, 1, 0));
	ExplosionNodeRight->Expand();
	AExplosionNode* ExplosionNodeLeft = GetWorld()->SpawnActor<AExplosionNode>(GetActorLocation(), FRotator(0, 0, 0));
	ExplosionNodeLeft->SetData(BombRadius, 100.0f, FVector(0,-1, 0));
	ExplosionNodeLeft->Expand();

	Destroy();

}
