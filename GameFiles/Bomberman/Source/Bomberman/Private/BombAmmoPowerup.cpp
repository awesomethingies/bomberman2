// Fill out your copyright notice in the Description page of Project Settings.


#include "BombAmmoPowerup.h"
#include "BombermanCharacter.h"

ABombAmmoPowerup::ABombAmmoPowerup()
{
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> AmmoPowerUpMaterial(TEXT("Material'/Game/Bomberman/Meshes/AmmoMat.AmmoMat'"));
	
	PowerUpMesh->SetMaterial(0, AmmoPowerUpMaterial.Object);
}

void ABombAmmoPowerup::Activate(ABombermanCharacter* character)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Ammo Powerup picked"));

	character->IncreaseBombAmmo(Value);
}
