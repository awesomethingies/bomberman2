// Fill out your copyright notice in the Description page of Project Settings.


#include "BombRemotePowerup.h"
#include "BombermanCharacter.h"

ABombRemotePowerup::ABombRemotePowerup()
{
	
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> RemotePowerUpMaterial(TEXT("Material'/Game/Bomberman/Meshes/RemoteMat.RemoteMat'"));

	PowerUpMesh->SetMaterial(0, RemotePowerUpMaterial.Object);
}

void ABombRemotePowerup::Activate(ABombermanCharacter* character)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Remote Powerup picked"));

	character->ActivateRemoteBomb();
}
