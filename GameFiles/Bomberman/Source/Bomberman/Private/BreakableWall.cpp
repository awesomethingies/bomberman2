// Fill out your copyright notice in the Description page of Project Settings.


#include "BreakableWall.h"
#include "..\Public\BreakableWall.h"
#include "PowerUpBase.h"
#include "BombRadiusPowerup.h"
#include "BombAmmoPowerup.h"
#include "MoveSpeedPowerup.h"
#include "BombRemotePowerup.h"

// Sets default values
ABreakableWall::ABreakableWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABreakableWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABreakableWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABreakableWall::BreakWall()
{

	int32 RandomValue = FMath::RandRange(1, 90);
	if (RandomValue <= 30)
	{
		GeneratePowerUp();
	}

	Destroy();
}

void ABreakableWall::GeneratePowerUp()
{

	int32 RandomValue = FMath::RandRange(0, 100);

	FVector spawnLocation = GetActorLocation();
	spawnLocation.Z += 50;

	//Simple Random Selection
	if (RandomValue < 25)
	{
		GetWorld()->SpawnActor<ABombRemotePowerup>(spawnLocation, FRotator(0, 0, 0));
	}
	if (RandomValue >= 25 && RandomValue < 50)
	{
		GetWorld()->SpawnActor<AMoveSpeedPowerup>(spawnLocation, FRotator(0, 0, 0));
	}
	if (RandomValue >= 50 && RandomValue < 75)
	{
		GetWorld()->SpawnActor<ABombAmmoPowerup>(spawnLocation, FRotator(0, 0, 0));
	}
	if (RandomValue >= 75)
	{
		GetWorld()->SpawnActor<ABombRadiusPowerup>(spawnLocation, FRotator(0, 0, 0));
	}
}

