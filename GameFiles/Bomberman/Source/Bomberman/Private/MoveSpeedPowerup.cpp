// Fill out your copyright notice in the Description page of Project Settings.


#include "MoveSpeedPowerup.h"
#include "BombermanCharacter.h"

AMoveSpeedPowerup::AMoveSpeedPowerup()
{
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> SpeedPowerUpMaterial(TEXT("Material'/Game/Bomberman/Meshes/SpeedMat.SpeedMat'"));

	PowerUpMesh->SetMaterial(0, SpeedPowerUpMaterial.Object);
}

void AMoveSpeedPowerup::Activate(ABombermanCharacter* character)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Speed Powerup picked"));

	character->IncreaseSpeed(Value);
}
