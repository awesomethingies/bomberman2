// Fill out your copyright notice in the Description page of Project Settings.


#include "BombermanCharacter.h"
#include "Bomb.h"
#include "BombRemote.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABombermanCharacter::ABombermanCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Camera Setup
	//CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	//CameraSpringArm->SetupAttachment(RootComponent);
	//CameraSpringArm->SetUsingAbsoluteRotation(true); 
	//CameraSpringArm->TargetArmLength = 1200.f;
	//CameraSpringArm->SetRelativeRotation(FRotator(-80.f, 0.f, 0.f));
	//CameraSpringArm->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	//Camera = CreateDefaultSubobject< UCameraComponent>(TEXT("Camera"));
	//Camera->SetupAttachment(CameraSpringArm, USpringArmComponent::SocketName);
	//Camera->bUsePawnControlRotation = false; //Avoid rotating Camera

}

// Called when the game starts or when spawned
void ABombermanCharacter::BeginPlay()
{
	Super::BeginPlay();
	

	BombAmmo = MaxBombAmmo;
}

// Called every frame
void ABombermanCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (RemoteBombUsage) 
	{
		if (RemoteBombTimer >= RemoteBombTime)
		{
			//Remove Remote bomb and explode remote bomb
			RemoteBombUsage = 0;
			if (RemoteBomb != nullptr)
				RemoteBomb->Explode();
		}
		RemoteBombTimer += DeltaTime;
	}
}

// Called to bind functionality to input
void ABombermanCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("DropBomb_Id0"), EInputEvent::IE_Pressed, this, &ABombermanCharacter::DropBomb);

	PlayerInputComponent->BindAxis(TEXT("MoveForward_Id0"), this, &ABombermanCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight_Id0"), this, &ABombermanCharacter::MoveRight);
}

void ABombermanCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		FRotator Rotation(0, Controller->GetControlRotation().Yaw, 0);

		AddMovementInput(UKismetMathLibrary::GetForwardVector(Rotation), Value);
	}
}

void ABombermanCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		FRotator Rotation(0, Controller->GetControlRotation().Yaw, 0);

		AddMovementInput(UKismetMathLibrary::GetRightVector(Rotation), Value);
	}

}

void ABombermanCharacter::DropBomb()
{
	
	FVector SpawnLocation = GetActorLocation();

	SpawnLocation.X = FMath::RoundToInt(SpawnLocation.X / TileLenght) * TileLenght;
	SpawnLocation.Y = FMath::RoundToInt(SpawnLocation.Y / TileLenght) * TileLenght;
	SpawnLocation.Z = 50.0f;


	if (BombAmmo > 0 && !RemoteBombUsage)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Bomb dropped"));

		ABomb* NewBomb = GetWorld()->SpawnActor<ABomb>(SpawnLocation, FRotator(0, 0, 0));
		NewBomb->SetRadius(BombRadius);
		NewBomb->OnBombExplosionEvent.AddDynamic(this, &ABombermanCharacter::RefillBomb);
		BombAmmo--;
	}

	if (RemoteBombUsage)
	{
		if (RemoteBomb == nullptr)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Black, TEXT("Remote Bombed Dropped"));

			RemoteBomb = GetWorld()->SpawnActor<ABombRemote>(SpawnLocation, FRotator(0, 0, 0));
			RemoteBomb->SetRadius(BombRadius);
		}
		else
		{
			RemoteBomb->Explode();
			RemoteBomb = nullptr;
		}
	}
}

void ABombermanCharacter::RefillBomb(bool Value)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Purple, TEXT("Bomb Refill"));

	if (BombAmmo < MaxBombAmmo)
		BombAmmo++;
}

void ABombermanCharacter::IncreaseBombRadius(int32 Value)
{
	BombRadius += Value;
}

void ABombermanCharacter::IncreaseBombAmmo(int32 Value)
{
	MaxBombAmmo += Value;
	BombAmmo += Value;
}

void ABombermanCharacter::IncreaseSpeed(float Value)
{
	GetCharacterMovement()->MaxWalkSpeed += Value;
}

void ABombermanCharacter::ActivateRemoteBomb()
{
	RemoteBombUsage = true;
	RemoteBombTimer = 0;
}



