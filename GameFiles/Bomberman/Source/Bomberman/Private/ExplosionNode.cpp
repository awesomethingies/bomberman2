// Fill out your copyright notice in the Description page of Project Settings.


#include "ExplosionNode.h"
#include "BombermanCharacter.h"
#include "Bomb.h"
#include "UnbreakableWall.h"
#include "BreakableWall.h"
#include "PowerUpBase.h"

// Sets default values
AExplosionNode::AExplosionNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> TileMeshAsset(TEXT("StaticMesh'/Game/Bomberman/Meshes/Tile.Tile'"));

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> ExplosionMaterial(TEXT("Material'/Game/Bomberman/Meshes/ExplosionRed.ExplosionRed'"));

	ExplosionMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BombMesh"));
	ExplosionMesh->SetStaticMesh(TileMeshAsset.Object);
	ExplosionMesh->SetupAttachment(RootComponent);
	ExplosionMesh->SetCollisionProfileName(TEXT("OverlapAll"));
	//ExplosionMesh->OnComponentBeginOverlap.AddDynamic(this, &AExplosionNode::OnBeginOverlap);

	ExplosionMesh->SetMaterial(0, ExplosionMaterial.Object);

	ExplosionMesh->SetRelativeScale3D(FVector(0.5, 0.5, 0.5));

	//AddActorLocalRotation(FRotator(45, 45, 45));
	//SetActorScale3D(FVector(0.5, 0.5, 0.5));

	InitialLifeSpan = 0.5f;
}

void AExplosionNode::SetData(int _ExplosionForce, float _TileLenght, FVector _Direction)
{
	ExplosionForce = _ExplosionForce;
	TileLenght = _TileLenght;
	Direction = _Direction;
}

// Called when the game starts or when spawned
void AExplosionNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AExplosionNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AExplosionNode::Expand()
{
	TArray<AActor*> OverlappingActors;
	ExplosionMesh->GetOverlappingActors(OverlappingActors);

	if (OverlappingActors.Num() >= 1)
	{
	
		for (int32 i = 0; i < OverlappingActors.Num(); ++i)
		{

			//Player Case
			if (OverlappingActors[i]->IsA<ABombermanCharacter>())
			{
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Emerald, TEXT("Player died"));
			}

			//BombCase
			if (OverlappingActors[i]->IsA<ABomb>())
			{
				CastChecked<ABomb>(OverlappingActors[i])->Explode();
			}

			//UnbreakableWall: Stop propagation at this node
			if (OverlappingActors[i]->IsA<AUnbreakableWall>())
			{
				ExplosionForce = 0;
			}

			//BreakableWall: Destroy the wall
			if (OverlappingActors[i]->IsA<ABreakableWall>())
			{
				CastChecked<ABreakableWall>(OverlappingActors[i])->BreakWall();
				ExplosionForce = 0;
			}

			if (OverlappingActors[i]->IsA<APowerUpBase>())
			{
				CastChecked<APowerUpBase>(OverlappingActors[i])->Destroy();
			}
		}
	}

	if (ExplosionForce > 1)
	{

		FVector NewLocation = GetActorLocation();
		NewLocation += Direction * TileLenght;
		AExplosionNode* ExplosionNode = GetWorld()->SpawnActor<AExplosionNode>(NewLocation, FRotator(0, 0, 0));
		ExplosionNode->SetData(--ExplosionForce, TileLenght, Direction);
		ExplosionNode->Expand();
	}
}

