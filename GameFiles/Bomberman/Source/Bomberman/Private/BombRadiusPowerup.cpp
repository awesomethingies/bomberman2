// Fill out your copyright notice in the Description page of Project Settings.


#include "BombRadiusPowerup.h"
#include "..\Public\BombRadiusPowerup.h"
#include "BombermanCharacter.h"

ABombRadiusPowerup::ABombRadiusPowerup()
{

	static ConstructorHelpers::FObjectFinder<UMaterialInterface> RadiusPowerUpMaterial(TEXT("Material'/Game/Bomberman/Meshes/RadiusMat.RadiusMat'"));

	PowerUpMesh->SetMaterial(0, RadiusPowerUpMaterial.Object);
}

void ABombRadiusPowerup::Activate(ABombermanCharacter* character)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Radius Powerup picked"));

	character->IncreaseBombRadius(Value);
}
