# Bomberman2

Simple bomberman copy project with unreal

## Controls

- Use WASD to move the manequin
- Use LeftControl to drop a bomb

## Not owned content
All content created is inside the Content/Bomberman folder, all other folders outside this are generic assets that come with unreal by default.    


An excepcion to this is the Content/Bomberman/Meshes folder, which contains Meshes and Materials taken from the genric assets, and in some cases modified. They where placed inside the Bomberman folder for better organitzation.


## Features Left
Here is a list of features that i could not implement

- Basic UI, and all features that came with it (Show win scree, display timer, ...)
- Respawn, due to this player dead has been left unimplmented as it would be pointless
- Couch-Coop

## Next steps
Here are some next steps that I would do to improve the code    

- Revise Unreal variable properties and definitions, that way I can hide some variables that where left public in this test due to 
issues with their accesabilty in blueprint/Editor. Also made the necesary Getters/Setters to edit them if necesary.    

- Replace the explosion mesh with VFX, possibly making diferent effects depending if it's the start or the end of the explosion.  

- Create a Grid Object which can handle bomb placement and tile definition, this way it will be possible to extract the bomb placement from the player, and it will be much easier to change tiles from broken to powerup, plus generate procedural maps.

- Add the missing second player   

- Add missing UI   
 
- Add player dead and respawn    

- Add some enemies and Basic AI  

- Add sound

- Make an approach to adapt the couch-coop to online multiplayer (or at least local LAN)

## Time spend
Around 19 hours were dedicated to this project, at the time of this commit
